#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from PySide2.QtCore import QCoreApplication
from PySide2.QtWidgets import QApplication
from MainWidget import MainWidget

def _start():
    QCoreApplication.setOrganizationName('STOEBER Antriebstechnik')
    QCoreApplication.setOrganizationDomain('stoeber.de')
    QCoreApplication.setApplicationName('PyCam')
    QCoreApplication.setApplicationVersion('1.0.0.0')

    app = QApplication(sys.argv)
    
    widget = MainWidget()
    widget.show()

    sys.exit(app.exec_())

if __name__ == "__main__":
    _start()
