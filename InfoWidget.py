#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QWidget

from Ui_InfoWidget import Ui_infoWidget



class InfoWidget(QWidget):

    def __init__(self):
        QWidget.__init__(self)
        
        #Attributes
        self.ui = Ui_infoWidget()
        
        # Set up the user interface from Designer.
        self.ui.setupUi(self)
