#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtCore import Signal,  Slot
from PySide2.QtWidgets import QVBoxLayout,  QWidget
from PySide2.QtMultimedia import QCamera, QCameraImageCapture, QCameraInfo
from PySide2.QtMultimediaWidgets import QCameraViewfinder

class CameraWidget(QWidget):
    
    errormessage = Signal(str)
    
    def __init__(self,  cameraNumber = 0):
        QWidget.__init__(self)
        
        self.onlineWebcams = QCameraInfo.availableCameras()
        if len(self.onlineWebcams) >  cameraNumber :
            self._initCam(cameraNumber)
        else:
            self.errormessage.emit("No Camera {} found".format(cameraNumber))
            
    def makeShot(self):
        capture = QCameraImageCapture(self.webcam)
        capture.capture(self.shotfolder)
        self.webcam.unlock()
        
    def screenshotfolder(self):
        return self.shotfolder
        
    def setScreenshotfolder(self,  foldername):
        self.shotfolder = foldername
        
    @Slot(int,  str)
    def _printResult(self,  id,  filename):
        print(id, filename)
        
    def _showView(self,  cameraNumber):
        self.campic = QCameraViewfinder()
    
        layout = QVBoxLayout()
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self.campic)
        self.setLayout(layout)
        
        self._webcam(cameraNumber)
            
    def _webcam(self, i):
        self.webcam = QCamera(self.onlineWebcams[i])
        self.webcam.setViewfinder(self.campic)
        self.webcam.setCaptureMode(QCamera.CaptureStillImage)
        self.webcam.error.connect(lambda: self.alert(self.webcam.errorString()))
        self.webcam.start()
        
    def _initCam(self,  cameraNumber):
        self._showView(cameraNumber)
        self.capture = QCameraImageCapture(self.webcam)
        # connections
        self.capture.imageSaved.connect(self._printResult)
