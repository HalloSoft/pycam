#!/usr/bin/python
# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QStackedLayout, QWidget
from PySide2.QtCore import QSettings,  Qt,  Slot
from Ui_MainWidget import Ui_Form
from CameraWidget import CameraWidget
from InfoWidget import InfoWidget

class MainWidget(QWidget):

    widgetNo = {'cam': 0, 'info': 1}
    
    def __init__(self):
        QWidget.__init__(self)
        
        #Attributes
        self.ui = Ui_Form()
        
        # Set up the user interface from Designer.
        self.ui.setupUi(self)
        
        self.layout = QStackedLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)
        
        cam = CameraWidget()
        self.layout.addWidget(cam)
        
        info = InfoWidget()
        self.layout.addWidget(info)
       
        self.setLayout(self.layout)
        
        #connections
        camwidget = self.layout.widget(self.widgetNo['cam'])
        #r = camwidget.errormessage.connect(lambda msg: print(msg))
        r = camwidget.errormessage.connect(self._printmessage) # TODO: find out why it is not working 
        assert r
        
         # settings
        settings = QSettings(self)
        shotfolder = settings.value('shotfolder',  r'c:')
        self.layout.widget(self.widgetNo['cam']).setScreenshotfolder(shotfolder)

    def keyPressEvent(self,  event):
        if event.key() == Qt.Key_T:
            self._toggleView()
        if event.key() == Qt.Key_S:
            self._shoot()
            
    def closeEvent(self,  event):
        shotfolder = self.layout.widget(self.widgetNo['cam']).screenshotfolder()
        settings = QSettings(self)
        settings.setValue('shotfolder',  shotfolder)
        event.accept()
        
    def _toggleView(self):
        if self.layout.currentIndex() == self.widgetNo['cam']:
            self.layout.setCurrentIndex(self.widgetNo['info'])
        else:
            self.layout.setCurrentIndex(self.widgetNo['cam'])
            
    def _shoot(self):
        camWidget = self.layout.widget(self.widgetNo['cam'])
        camWidget.makeShot()
        
    @Slot()
    def _printmessage(self,  message):
        print(message)
            
